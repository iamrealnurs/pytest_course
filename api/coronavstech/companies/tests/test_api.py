import json
import pytest
from unittest import TestCase
from django.test import Client
from django.urls import reverse
from companies.models import Company

companies_url = reverse('companies-list')
pytestmark = pytest.mark.django_db


# Test get companies
def test_zero_companies_should_return_empty_list(client)->None:
    response = client.get(companies_url)
    assert response.status_code == 200
    assert json.loads(response.content) == []

def test_one_company_exists_should_succeed(client) -> None:
    amazon = Company.objects.create(name="Amazon")
    response = client.get(companies_url)
    response_content = json.loads(response.content)[0]

    assert response.status_code == 200
    assert response_content.get('id') == 1
    assert response_content.get('name') == amazon.name
    assert response_content.get('status') == amazon.status
    assert response_content.get('application_link') == amazon.application_link
    assert response_content.get('application_link') == ''
    assert response_content.get('notes') == amazon.notes
    assert response_content.get('notes') == ''
    amazon.delete()

# Test post companies
def test_create_company_without_arguments_should_fall(client) -> None:
    response = client.post(path=companies_url)
    assert response.status_code == 400
    assert json.loads(response.content) == {"name": ["This field is required."]}

def test_create_existing_company_should_fall(client) -> None:
    Company.objects.create(name="ASU")
    response = client.post(
        path=companies_url,
        data={"name":"ASU"}
    )
    assert response.status_code, 400
    assert json.loads(response.content) == {"name": ["company with this name already exists."]}

def test_create_company_with_only_name_all_fields_should_be_default(client) -> None:
    response = client.post(
        path=companies_url,
        data={"name":"Xiaomi"}
    )
    response_content = json.loads(response.content)
    assert response.status_code == 201
    assert response_content.get('id') == 1
    assert response_content.get('name') == "Xiaomi"
    assert response_content.get('status') == "Hiring"
    assert response_content.get('application_link') == ''
    assert response_content.get('notes') == ''

def test_create_company_with_only_name_and_layoffs_status_all_fields_should_be_default(client) -> None:
    response = client.post(
        path=companies_url,
        data={"name":"Xiaomi", "status":"Layoffs"}
    )
    response_content = json.loads(response.content)
    assert response.status_code == 201
    assert response_content.get('id') == 1
    assert response_content.get('name') == "Xiaomi"
    assert response_content.get('status') == "Layoffs"
    assert response_content.get('application_link') == ''
    assert response_content.get('notes') == ''

def test_create_company_with_only_name_and_wrong_status_should_fail(client) -> None:
    response = client.post(
        path=companies_url,
        data={"name":"Xiaomi", "status":"Wrong"}
    )
    response_content = json.loads(response.content)
    assert response.status_code == 400
    assert json.loads(response.content) == {"status": ["\"Wrong\" is not a valid choice."]}
    assert  "Wrong" in str(response_content)
    assert  "is not a valid choice." in str(response_content)

