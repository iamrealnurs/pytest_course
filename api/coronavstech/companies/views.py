from rest_framework.viewsets import ModelViewSet
from rest_framework.pagination import PageNumberPagination

from django.shortcuts import render
from .serializers import CompanySerializer
from .models import Company

class CompanyViewSet(ModelViewSet):
    serializer_class = CompanySerializer
    queryset = Company.objects.all().order_by('-last_update')
    pagintaion_class = PageNumberPagination
